package com.nisum.selectionprocessexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelectionProcessExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SelectionProcessExerciseApplication.class, args);
	}

}
