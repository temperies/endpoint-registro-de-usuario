package com.nisum.selectionprocessexercise.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@RequestMapping(TestController.CTX)
public interface TestController {
    String CTX = "/tests";
    String CHECK_URL = "/check";

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(CHECK_URL)
    String checkAuthenticated();
}
