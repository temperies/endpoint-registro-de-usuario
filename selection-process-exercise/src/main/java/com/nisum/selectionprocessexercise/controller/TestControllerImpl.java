package com.nisum.selectionprocessexercise.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestControllerImpl implements TestController {

    @Override
    public String checkAuthenticated() {
        return "checked!";
    }
}
