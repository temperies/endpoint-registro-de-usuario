package com.nisum.selectionprocessexercise.repository;


import com.nisum.selectionprocessexercise.model.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CityRepository extends JpaRepository<City, Long> {

  List<City> findByCountryName(String name);
  Page<City> findByCountryName(String name, Pageable pageable);
  List<City> findByCountryId(long id);


}
