package com.nisum.selectionprocessexercise.repository;


import com.nisum.selectionprocessexercise.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
