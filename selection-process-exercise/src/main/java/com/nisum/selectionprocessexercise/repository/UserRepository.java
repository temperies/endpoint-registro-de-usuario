package com.nisum.selectionprocessexercise.repository;


import com.nisum.selectionprocessexercise.model.NisumUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Repository
public interface UserRepository extends JpaRepository<NisumUser, UUID> {

  @Modifying
  @Query("UPDATE NisumUser u SET u.active = false WHERE u.lastLoginDate < :date")
  void deactivateUsersNotLoggedInSince(@Param("date") LocalDate date);

  @Modifying
  @Query("DELETE NisumUser u WHERE u.active = false")
  int deleteDeactivatedUsers();

  List<NisumUser> findByActiveTrue();

  Optional<NisumUser> findByName(String name);

  boolean existsByName(String name);

  boolean existsByEmail(String email);

  Optional<NisumUser> findByEmail(String email);

}
