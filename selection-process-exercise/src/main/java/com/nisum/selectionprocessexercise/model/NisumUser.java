package com.nisum.selectionprocessexercise.model;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(	name = "users")
public class NisumUser {

  @Id
  @GeneratedValue
  private UUID id;

  @Column(name = "name")
  private String name;
  @Column(name = "email", unique = true)
  private String email;
  private String password;
  private boolean active;

  @OneToMany(mappedBy = "user")
  private Set<Phone> phones;

  @CreatedDate
  private LocalDate creationDate;
  @LastModifiedDate
  private LocalDate lastModifiedDate;
  private LocalDate lastLoginDate;



  public UUID getId() {

    return id;
  }



  public void setId(UUID id) {

    this.id = id;
  }



  public String getName() {

    return name;
  }



  public void setName(String name) {

    this.name = name;
  }



  public String getEmail() {

    return email;
  }



  public void setEmail(String email) {

    this.email = email;
  }



  public String getPassword() {

    return password;
  }



  public void setPassword(String password) {

    this.password = password;
  }

  public boolean isActive() {

    return active;
  }



  public void setActive(boolean active) {

    this.active = active;
  }



  public LocalDate getCreationDate() {

    return creationDate;
  }



  public void setCreationDate(LocalDate creationDate) {

    this.creationDate = creationDate;
  }



  public LocalDate getLastModifiedDate() {

    return lastModifiedDate;
  }



  public void setLastModifiedDate(LocalDate lastModifiedDate) {

    this.lastModifiedDate = lastModifiedDate;
  }



  public LocalDate getLastLoginDate() {

    return lastLoginDate;
  }



  public void setLastLoginDate(LocalDate lastLoginDate) {

    this.lastLoginDate = lastLoginDate;
  }



  public Set<Phone> getPhones() {

    return phones;
  }



  public void setPhones(Set<Phone> phones) {

    this.phones = phones;
  }
}
