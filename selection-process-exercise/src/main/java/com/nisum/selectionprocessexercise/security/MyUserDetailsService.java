package com.nisum.selectionprocessexercise.security;


import com.nisum.selectionprocessexercise.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

  private final UserRepository repository;

  public MyUserDetailsService(UserRepository repository) {
    this.repository = repository;
  }

  public @Override
  UserDetails loadUserByUsername(String email) {
    return repository.findByEmail(email)
        .map(SecureUserFactory::toUser)
        .orElseThrow(() -> new UsernameNotFoundException("No user found with username: " + email));
  }
}
