package com.nisum.selectionprocessexercise.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.nisum.selectionprocessexercise.repository")
@EnableJpaAuditing
@ComponentScan({ "com.nisum.selectionprocessexercise.repository" })
public class PersistenceConfig {

}
