package com.nisum.selectionprocessexercise.dto;


import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;


public class PhoneDto {

    @NotNull
    @NotBlank
    @Length(min = 6, message = "Invalid phone number")
    private String number;

    @NotNull
    @Length(min = 1, message = "Invalid city code")
    private String cityCode;

    @NotNull
    @Length(min = 1,message = "Invalid country code")
    private String countryCode;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneDto phoneDto = (PhoneDto) o;
        return Objects.equals(number, phoneDto.number) &&
                Objects.equals(cityCode, phoneDto.cityCode) &&
                Objects.equals(countryCode, phoneDto.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, cityCode, countryCode);
    }
}
