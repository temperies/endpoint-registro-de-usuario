package com.nisum.selectionprocessexercise.service;


import com.nisum.selectionprocessexercise.model.NisumUser;
import com.nisum.selectionprocessexercise.model.UserToken;
import com.nisum.selectionprocessexercise.repository.UserRepository;
import com.nisum.selectionprocessexercise.repository.UserTokenRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.nisum.selectionprocessexercise.utils.FactoryUtils.newUserDto;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {

  @Mock
  private UserRepository userRepository;
  @Mock
  private UserTokenRepository tokenRepository;
  @Mock
  private PasswordEncoder passwordEncoder;
  @InjectMocks
  private UserService userService = new UserServiceImpl();

  @Test void registerNewUserAccount() {

    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(passwordEncoder.encode(anyString())).thenReturn(new BCryptPasswordEncoder(11)
        .encode("P4ssw0rd"));
    final UserToken userTokenSaved = new UserToken("testToken");
    when(tokenRepository.save(any(UserToken.class))).thenReturn(userTokenSaved);

    final UserToken userToken = userService.registerNewUserAccount(newUserDto());

    Assert.assertEquals(userToken,userTokenSaved);
  }



  @Test
  void validateUserToken() {

    final UserToken savedToken = new UserToken("testToken");
    Date currentDate = new Date();
    LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusDays(1);
    savedToken.setExpirationDate(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()));
    final NisumUser savedUser = new NisumUser();
    savedToken.setUser(savedUser);
    when(tokenRepository.findByToken(anyString())).thenReturn(savedToken);

    final String testToken = userService.validateUserToken("testToken");

    Assert.assertEquals(UserService.TOKEN_VALID, testToken);
  }
}