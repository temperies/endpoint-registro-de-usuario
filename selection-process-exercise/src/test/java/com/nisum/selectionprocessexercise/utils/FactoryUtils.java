package com.nisum.selectionprocessexercise.utils;


import com.nisum.selectionprocessexercise.dto.UserDto;
import com.nisum.selectionprocessexercise.model.NisumUser;

import java.util.HashSet;
import java.util.UUID;


public class FactoryUtils {

  public static final UserDto newUserDto() {
    UserDto accountDto = new UserDto();
    accountDto.setEmail(UUID.randomUUID().toString() + "@myemail.com");
    accountDto.setName(" ");
    accountDto.setPassword("P4ssw0rd");
    accountDto.setPhones(new HashSet<>());
    return accountDto;
  }
}
