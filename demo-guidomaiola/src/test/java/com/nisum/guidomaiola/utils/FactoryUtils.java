package com.nisum.guidomaiola.utils;


import com.nisum.guidomaiola.dto.UserDto;
import com.nisum.guidomaiola.model.NisumUser;

import java.util.HashSet;
import java.util.UUID;


public class FactoryUtils {

  public static final UserDto newUserDto() {
    UserDto accountDto = new UserDto();
    accountDto.setEmail(UUID.randomUUID().toString() + "@myemail.com");
    accountDto.setName(" ");
    accountDto.setPassword("P4ssw0rd");
    accountDto.setPhones(new HashSet<>());
    return accountDto;
  }
}
