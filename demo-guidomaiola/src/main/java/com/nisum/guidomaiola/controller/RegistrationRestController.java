package com.nisum.guidomaiola.controller;


import com.nisum.guidomaiola.dto.UserDto;
import com.nisum.guidomaiola.dto.UserRegisteredDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RequestMapping(RegistrationRestController.CTX)
public interface RegistrationRestController {
    String CTX = "/users";
    String REGISTRATION_URL = "/register";

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(path = REGISTRATION_URL, consumes = MediaType.APPLICATION_JSON_VALUE)
    UserRegisteredDto registerUserAccount(@RequestBody @Valid final UserDto accountDto, final HttpServletRequest request);
}
