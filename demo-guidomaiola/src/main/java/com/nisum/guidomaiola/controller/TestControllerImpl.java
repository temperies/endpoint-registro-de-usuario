package com.nisum.guidomaiola.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestControllerImpl implements TestController {

    @Override
    public String checkAuthenticated() {
        return "checked!";
    }
}
