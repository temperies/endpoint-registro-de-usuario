package com.nisum.guidomaiola.dto;


import java.util.UUID;


public class UserRegisteredDto {
  private UUID uuid;
  private String creationDate;
  private String lastModifiedDate;
  private String lastLoginDate;
  private String token;
  private boolean active;


  public UserRegisteredDto() {
  }



  public UserRegisteredDto(String token) {

    this.token = token;
  }



  public UserRegisteredDto(UUID uuid, String creationDate, String lastModifiedDate,
      String lastLoginDate, String token, boolean active) {

    this.uuid = uuid;
    this.creationDate = creationDate;
    this.lastModifiedDate = lastModifiedDate;
    this.lastLoginDate = lastLoginDate;
    this.token = token;
    this.active = active;
  }



  public UUID getUuid() {

    return uuid;
  }



  public void setUuid(UUID uuid) {

    this.uuid = uuid;
  }



  public String getCreationDate() {

    return creationDate;
  }



  public void setCreationDate(String creationDate) {

    this.creationDate = creationDate;
  }



  public String getLastModifiedDate() {

    return lastModifiedDate;
  }



  public void setLastModifiedDate(String lastModifiedDate) {

    this.lastModifiedDate = lastModifiedDate;
  }



  public String getLastLoginDate() {

    return lastLoginDate;
  }



  public void setLastLoginDate(String lastLoginDate) {

    this.lastLoginDate = lastLoginDate;
  }



  public String getToken() {

    return token;
  }



  public void setToken(String token) {

    this.token = token;
  }



  public boolean isActive() {

    return active;
  }



  public void setActive(boolean active) {

    this.active = active;
  }
}
