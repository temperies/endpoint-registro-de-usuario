package com.nisum.guidomaiola.repository;

import com.nisum.guidomaiola.model.NisumUser;
import com.nisum.guidomaiola.model.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.stream.Stream;

public interface UserTokenRepository extends JpaRepository<UserToken, Long> {

    UserToken findByToken(String token);

    UserToken findByUser(NisumUser user);

    Stream<UserToken> findAllByExpirationDateLessThan(Date now);

    void deleteByExpirationDateLessThan(Date now);

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("DELETE FROM UserToken t WHERE t.expirationDate <= ?1")
    void deleteAllExpiredSince(Date now);
}
