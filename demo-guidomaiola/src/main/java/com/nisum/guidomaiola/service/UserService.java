package com.nisum.guidomaiola.service;


import com.nisum.guidomaiola.dto.UserDto;
import com.nisum.guidomaiola.model.NisumUser;
import com.nisum.guidomaiola.model.UserToken;

import java.util.Optional;
import java.util.UUID;


public interface UserService {
  String TOKEN_INVALID = "invalidToken";
  String TOKEN_EXPIRED = "expired";
  String TOKEN_VALID = "valid";

  UserToken registerNewUserAccount(UserDto accountDto);
  NisumUser getUser(String token);
  NisumUser findUserByEmail(String email);
  Optional<NisumUser> getUserByUUID(UUID uuid);
  String validateUserToken(String token);
  UserToken createTokenForUser(NisumUser user, String token);
  UserToken getUserToken(String token);
  UserToken generateNewUserToken(String existingUserToken);
}
