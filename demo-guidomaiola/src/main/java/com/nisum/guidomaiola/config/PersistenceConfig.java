package com.nisum.guidomaiola.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.nisum.guidomaiola.repository")
@EnableJpaAuditing
@ComponentScan({ "com.nisum.guidomaiola.repository" })
public class PersistenceConfig {

}
