package com.nisum.guidomaiola.exception;


public class SelectionProcessExerciseException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_CODE = "not-specific-error";

  private String code;


  public SelectionProcessExerciseException() {

  }



  public SelectionProcessExerciseException(String message, Throwable cause) {

    super(message, cause);
  }



  public SelectionProcessExerciseException(Throwable cause) {

    super(cause);
  }



  public SelectionProcessExerciseException(final String message, String code) {

    super(message);
    setCode(code);
  }



  public SelectionProcessExerciseException(final String message) {

    this(message, DEFAULT_CODE);
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
