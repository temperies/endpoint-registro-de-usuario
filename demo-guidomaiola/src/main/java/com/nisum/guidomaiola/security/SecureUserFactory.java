package com.nisum.guidomaiola.security;

import com.nisum.guidomaiola.model.NisumUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;

public class SecureUserFactory {

  private SecureUserFactory() {
  }

  public static User toUser(NisumUser user) {
    return new User(user.getEmail(), user.getPassword(), Collections.emptyList());
  }

  public static UsernamePasswordAuthenticationToken toAuthenticationToken(NisumUser user) {
    return new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword(), Collections.emptyList());
  }
}
