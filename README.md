# README #

# Evaluación: JAVA

Desarrolle una aplicación que exponga una API RESTful de creación de usuarios.

Todos los endpoints deben aceptar y retornar solamente JSON, inclusive al para los mensajes de error.

Todos los mensajes deben seguir el formato:

```json
    {"mensaje": "mensaje de error"}
```
## Registro
* Ese endpoint deberá recibir un usuario con los campos "nombre", "correo", "contraseña", más un listado de objetos "teléfono", respetando el siguiente formato:

```json
{
"name": "Juan Rodriguez",
"email": "juan@rodriguez.org",
"password": "hunter2",
"phones": [
{
"number": "1234567",
"citycode": "1",
"contrycode": "57"
}
]
}

```

Responder el código de status HTTP adecuado
+ En caso de éxito, retorne el usuario y los siguientes campos:
	+ id: id del usuario (puede ser lo que se genera por el banco de datos, pero sería más deseable un UUID)
	+ created: fecha de creación del usuario
	+ modified: fecha de la última actualización de usuario
	+ last_login: del último ingreso (en caso de nuevo usuario, va a coincidir con la fecha de creación)
	+ token: token de acceso de la API (puede ser UUID o JWT)
	+ isactive: Indica si el usuario sigue habilitado dentro del sistema.
+ Si caso el correo conste en la base de datos, deberá retornar un error "El correo ya registrado".
+ El correo debe seguir una expresión regular para validar que formato sea el correcto.
(aaaaaaa@dominio.cl)
+ La clave debe seguir una expresión regular para validar que formato sea el correcto. (Una Mayuscula, letras minúsculas, y dos numeros)
+ El token deberá ser persistido junto con el usuario

## Requisitos

+ Plazo: 2 días
+ Banco de datos en memoria, como HSQLDB o H2.
+ Proceso de build via Gradle.
+ Persistencia con Hibernate.
+ Framework Spring.
+ Servidor Tomcat o Jetty Embedded
+ Java 8+
+ Entrega en un repositorio público (github o bitbucket) con el código fuente y script de creación de BD.
+ Entrega diagrama de la solución.

## Requisitos deseables

+ JWT cómo token
+ Pruebas de unidad


------------

------------



### What is this repository for? ###

* [Learn Markdown](https://bitbucket.org/temperies/endpoint-registro-de-usuario/src/master/)


### Contribution guidelines ###

* Test Cases
There were created live integration and unit tests.
Live tests were tagged as live and are disable in the build process

### Extras ###
* Password matching on registration request
* Internationalization
* Endpoint to verify the correct generation of the token.
* Live integration Test

####Example

**Registration**

***REQUEST***

***HTTP Verb***
> POST

***Header***
> Content-Type:application/json

***Body***
```json
{
	"name":"Name Surname",
	"password":"P4ssw0rd",
	"matchingPassword": "P4ssw0rd",
	"email": "email@example.cl",
	"phones": [
		{
			"number": "1234567",
			"countryCode": "054",
			"cityCode": "0249"
		}
	]
}
```

***RESPONSE***
```json
{
    "uuid": "5ae33212-1e5b-4f5e-9711-1f12f2b6a2a7",
    "creationDate": "2020-08-28",
    "lastModifiedDate": "2020-08-28",
    "lastLoginDate": null,
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJlbWFpbEBleGFtcGxlLmNsIiwiZXhwIjoxNTk4NzQwMjEyfQ.TvKpoxjPglLYnAtkHTWoFHl96yQ1DiDiJTzuZefr9Yeb8kFHXmbrgtufVc7P4inUy5rk8Yw4HKgCDgVA3T5PJg",
    "active": true
}
```
---

> User Registration Secuence Diagram
![](https://cacoo.com/diagrams/RDmflnUGExJdF2kc-1E7AC.png)

### Who do I talk to? ###

* Developer
***Luis Pouzo***